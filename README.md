Originally a website I had written as an interactive resume for LIS4708, which was hosted on Github. I had written it using [Nuxt.js](https://nuxtjs.org/) because it made developing websites a lot more streamlined with it's use of definable layouts. However I had zero knowledge of Vue, which the framework utilized, so this isn't exactly representative of any existing skills with that language. While I intend to rewrite and republish the website under a language I'm more knowledgable on, I keep this older version available to represent the website in a state when I completed that course.

## Running the website locally
Simply install the dependencies (`npm i`) and run Nuxt in development mode (`npm run dev`) to be able to access the website through a local means.

### Third-party resources
- Source Sans, [provided by Adobe Fonts](https://fonts.adobe.com/fonts/source-sans)
- [Font Awesome v5.7.2 (free)](https://fontawesome.com)
