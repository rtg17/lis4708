module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - rtgordon.github.io',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'My portfolio website' }
    ],
    link: [
      { rel: 'stylesheet', href: 'https://use.typekit.net/ghd8enx.css' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.7.2/css/all.css',
        integrity: 'sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr', crossorigin: 'anonymous' }
    ]
  },

  //Progress bar color
  loading: { color: '#fff' },
  //Global CSS
  css: [
      'assets/global.css'
  ],
  //Plugins to load before mounting the App
  plugins: [
  ],
  //Nuxt modules
  modules: [
  ],
  //Build config
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {

    }
  }
}
